Something changed between npm 7.0.3 and 7.0.8 which causes npm publish \<folder> to use the folder name for the package instead of the name field in the package.json file

# With Node-v15.0.1-x64.msi installed

```
Microsoft Windows [Version 10.0.19042.1288]

C:\temp\publish\my-module>node --version && npm --version
v15.0.1
7.0.3

C:\temp\publish\my-module>npm run publish.current

> @xyzzy/my-module@1.0.0 publish.current
> cross-env npm run package && npm publish pub

> @xyzzy/my-module@1.0.0 package
> cross-env npm run pack && webpack --config package.webpack.config.js

> @xyzzy/my-module@1.0.0 pack
> babel src --out-dir pack --source-maps --copy-files --ignore _.js,_.vue

Successfully compiled 1 file with Babel (150ms).
assets by path dist/ 234 bytes
asset dist/index.js.map 179 bytes [compared for emit] [from: pack/index.js.map] [copied]
asset dist/index.js 55 bytes [compared for emit] [from: pack/index.js] [copied]
asset package 1.21 KiB [emitted] (name: main)
asset package.json 568 bytes [compared for emit] [from: package.json] [copied]
asset src/index.js 22 bytes [compared for emit] [from: src/index.js] [copied]
./src/index.js 22 bytes [built] [code generated]
webpack 5.60.0 compiled successfully in 84 ms
npm notice
npm notice 📦 @xyzzy/my-module@1.0.0
npm notice === Tarball Contents ===
npm notice 1.1kB LICENSE.mkd
npm notice 492B README.mkd
npm notice 14B .npmignore
npm notice 207B .project
npm notice 0B bin/
npm notice 1.1kB bin/pub
npm notice 709B package.json
npm notice 759B pub.js
npm notice === Tarball Details ===
npm notice name: @xyzzy/my-module
npm notice version: 1.0.0
npm notice filename: @xyzzy/my-module-1.0.0.tgz
npm notice package size: 2.4 kB
npm notice unpacked size: 4.4 kB
npm notice shasum: 3f8f2d61f87ecea5044eda18a07b03caf7bc87c8
npm notice integrity: sha512-XRFNVv4v35Z7R[...]sUsWP012XlFSw==
npm notice total files: 8
```

# node-v15.1.0-x64.msi

```
C:\temp\publish\my-module>node --version & npm --version
v15.1.0
7.0.8

C:\temp\publish\my-module>npm run publish.current

> @xyzzy/my-module@1.0.0 publish.current
> cross-env npm run package && npm publish pub

> @xyzzy/my-module@1.0.0 package
> cross-env npm run pack && webpack --config package.webpack.config.js

> @xyzzy/my-module@1.0.0 pack
> babel src --out-dir pack --source-maps --copy-files --ignore _.js,_.vue

Successfully compiled 1 file with Babel (162ms).
assets by path dist/ 234 bytes
asset dist/index.js.map 179 bytes [compared for emit] [from: pack/index.js.map] [copied]
asset dist/index.js 55 bytes [compared for emit] [from: pack/index.js] [copied]
asset package 1.21 KiB [compared for emit] (name: main)
asset package.json 568 bytes [compared for emit] [from: package.json] [copied]
asset src/index.js 22 bytes [compared for emit] [from: src/index.js] [copied]
./src/index.js 22 bytes [built] [code generated]
webpack 5.60.0 compiled successfully in 75 ms
npm notice
npm notice 📦 pub@0.2.0
npm notice === Tarball Contents ===
npm notice 1.1kB LICENSE.mkd
npm notice 492B README.mkd
npm notice 14B .npmignore
npm notice 207B .project
npm notice 0B bin/
npm notice 1.1kB bin/pub
npm notice 709B package.json
npm notice 759B pub.js
npm notice === Tarball Details ===
npm notice name: pub
npm notice version: 0.2.0
npm notice filename: pub-0.2.0.tgz
npm notice package size: 2.4 kB
npm notice unpacked size: 4.4 kB
npm notice shasum: 3f8f2d61f87ecea5044eda18a07b03caf7bc87c8
npm notice integrity: sha512-XRFNVv4v35Z7R[...]sUsWP012XlFSw==
npm notice total files: 8
```
