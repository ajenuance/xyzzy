const CopyWebpackPlugin = require("copy-webpack-plugin");
const path = require("path");

module.exports = {
  mode: "development",
  entry: "./src/index.js",
  output: {
    path: path.resolve(__dirname, "pub"),
    filename: "package"
  },
  plugins: [
    new CopyWebpackPlugin({
      patterns: [
        { from: "src", to: "src" },
        { from: "pack", to: "dist" },
        { from: "package.json", to: "package.json" }
      ]
    })
  ]
};
